# BoltzTraP2 jupyter notebook

# Last update: 17-Mar-2021
# Created by Gavin Woolman

# Idea:
# Reads in BoltzTraP or BoltzTraP2 output data from .trace, .condtens, or .halltens files
# Saves them in a handy python3 dict object, for easy plotting using matplotlib

# Requirements:
#               - Python3
#               - numpy
#               - matplotlib (or other python plotting library)

# Jupyter notebooks are only needed if one desires to use the .ipynb file.
